import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task010 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        boolean y;
        if (x <= 0)
            y = false;
        else
            y = true;
        System.out.println(y);
    }
}
