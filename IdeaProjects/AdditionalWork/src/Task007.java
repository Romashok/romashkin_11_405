import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task007 {
    public static void main(String[] args) {
        Random r = new Random();
        boolean A = r.nextBoolean(), B = r.nextBoolean(), C = r.nextBoolean(), Zn;
        Zn = !A & B;
        System.out.println(Zn);
        Zn = A | !B;
        System.out.println(Zn);
        Zn = A & B | C;
        System.out.println(Zn);

    }
}
