/**
 * Created by Romash on 20.11.2014.
 */
public class Task031 {
    public static void main(String[] args) {
        int a = 7, r = 0;
        for (int i = 1; i <= a; i++) {
            r = i * a;
            System.out.print(i + "x" + a + "=" + r + "\n");
        }
    }
}
