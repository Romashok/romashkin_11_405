import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task001 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt(), y = 0;
        y = 2 * x + x;
        System.out.println(y);
    }
}
