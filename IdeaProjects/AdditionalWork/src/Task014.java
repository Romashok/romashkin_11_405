import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task014 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        if (x > -5 & x < 3)
            System.out.println("удовлетворяет");
        else
            System.out.println("не удовлетворяет");
    }
}
