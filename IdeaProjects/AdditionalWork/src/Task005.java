import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task005 {
    public static void main(String[] args) {
        Random r = new Random();
        int n = r.nextInt(4235), h = 0, m = 0, s = 0;
        h = n / 3600;
        m = (n - h * 3600) / 60;
        s = (n - h * 3600) % 60;
        System.out.println("С начала суток прошло " + h + " часов, с начала часа " + m + " минут и с начала минуты " + s + " секунд");
        System.out.println(n);
    }
}
