import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task023 {
    public static void main(String[] args) {
        Random r = new Random();
        int a = r.nextInt(100), b = r.nextInt(100), c = r.nextInt(100), s = 0;
        if (a <= c & b <= c)
            s = a * b;
        else if (a <= b & c <= b)
            s = a * c;
        else if (b <= a & c <= a)
            s = b * c;
        System.out.println(s);
        System.out.println(a + " " + b + " " + c);
    }
}
