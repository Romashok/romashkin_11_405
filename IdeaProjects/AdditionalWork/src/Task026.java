import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task026 {
    public static void main(String[] args) {
        Random r = new Random();
        int g = r.nextInt(2015), n = 0, m = r.nextInt(12), gg = 0, nn = 0, mm = 0;
        if (m == 0) {
            while (m == 0) {
                m = r.nextInt(12);
            }
        }
        if (g == 0) {
            while (g == 0) {
                g = r.nextInt(2015);
            }
        }
        if (g % 100 == 0) {
            if (g % 400 == 0) {
                System.out.println(g + "-високосный год");
                if (m == 2) {
                    n = r.nextInt(28);
                    if (n == 0) {
                        while (n == 0) {
                            n = r.nextInt(28);
                        }
                    }
                }
            } else {
                System.out.println(g + "-не високосный год");
                if (m == 2) {
                    n = r.nextInt(29);
                    if (n == 0) {
                        while (n == 0) {
                            n = r.nextInt(29);
                        }
                    }
                }
            }
        } else if (g % 4 == 0) {
            System.out.println(g + "-високосный год");
            if (m == 2) {
                n = r.nextInt(28);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(28);
                    }
                }
            }
        } else {
            System.out.println(g + "-не високосный год");
            if (m == 2) {
                n = r.nextInt(29);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(28);
                    }
                }
            }
        }
        switch (m) {
            case 1:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 3:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 4:
                n = r.nextInt(30);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(30);
                    }
                }
                break;
            case 5:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 6:
                n = r.nextInt(30);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(30);
                    }
                }
                break;
            case 7:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 8:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 9:
                n = r.nextInt(30);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(30);
                    }
                }
                break;
            case 10:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
                break;
            case 11:
                n = r.nextInt(30);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(30);
                    }
                }
                break;
            case 12:
                n = r.nextInt(31);
                if (n == 0) {
                    while (n == 0) {
                        n = r.nextInt(31);
                    }
                }
        }
        System.out.println("Дата:" + n + "." + m + "." + g);
        nn = n;
        mm = m;
        gg = g;
        if (n - 1 == 0) {
            m = m - 1;
            switch (m) {
                case 11:
                    n = 30;
                    break;
                case 10:
                    n = 31;
                    break;
                case 9:
                    n = 30;
                    break;
                case 8:
                    n = 31;
                    break;
                case 7:
                    n = 31;
                    break;
                case 6:
                    n = 30;
                    break;
                case 5:
                    n = 31;
                    break;
                case 4:
                    n = 30;
                    break;
                case 3:
                    n = 31;
                    break;
                case 2:
                    if (g % 100 == 0) {
                        if (g % 400 == 0) {
                            n = 28;
                        } else {
                            n = 29;
                        }
                    } else if (g % 4 == 0) {
                        n = 28;
                    } else {
                        n = 29;
                    }
                    break;
                case 1:
                    n = 31;
                    break;
                case 0:
                    m = 12;
                    n = 31;
                    g = g - 1;
                    break;
            }
        } else
            n = n - 1;
        System.out.println("Дата вчерашнего дня:" + n + "." + m + "." + g);
        n = nn;
        m = mm;
        g = gg;
        if ((n + 1 == 29) & (m == 2) & (((g % 400 == 0) & (g % 100 == 0)) | (g % 4 == 0))) {
            m = 3;
            n = 1;
            System.out.println("Дата завтрашнего дня:" + n + "." + m + "." + g);
        } else if ((n + 1 == 30) & (m == 2) & (((g % 400 != 0) & (g % 100 != 0)) | (g % 4 != 0))) {
            m = 3;
            n = 1;
            System.out.println("Дата завтрашнего дня:" + n + "." + m + "." + g);
        } else if ((n + 1 == 31) & (m == 11 | m == 9 | m == 6 | m == 4)) {
            switch (m) {
                case 11:
                    m = 12;
                    n = 1;
                    break;
                case 9:
                    m = 10;
                    n = 1;
                    break;
                case 6:
                    m = 7;
                    n = 1;
                    break;
                case 4:
                    m = 5;
                    n = 1;
                    break;
            }
            System.out.println("Дата завтрашнего дня:" + n + "." + m + "." + g);
        } else if (n + 1 == 32) {
            switch (m) {
                case 1:
                    m = 2;
                    n = 1;
                    break;
                case 3:
                    m = 4;
                    n = 1;
                    break;
                case 5:
                    m = 6;
                    n = 1;
                    break;
                case 7:
                    m = 8;
                    n = 1;
                    break;
                case 8:
                    m = 9;
                    n = 1;
                    break;
                case 10:
                    m = 11;
                    n = 1;
                    break;
                case 12:
                    g = g + 1;
                    m = 1;
                    n = 1;
                    break;
            }

            System.out.println("Дата завтрашнего дня:" + n + "." + m + "." + g);
        } else {
            n = n + 1;
            System.out.println("Дата завтрашнего дня:" + n + "." + m + "." + g);
        }


    }


}
