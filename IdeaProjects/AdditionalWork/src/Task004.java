import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task004 {
    public static void main(String[] args) {
        Random rand = new Random();
        int r = rand.nextInt(100), d = 0;
        d = 2 * r;
        System.out.println("Если радиус окружности равен:" + r + " " + "то ее диаметр равен:" + d);
    }
}
