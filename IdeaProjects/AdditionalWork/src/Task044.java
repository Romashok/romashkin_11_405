import java.util.Scanner;

/**
 * Created by Romash on 22.11.2014.
 */
public class Task044 {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int n = 7;
        Double a = 0.00, b = 0.00;
        for (int i = 0; i < n; i++) {
            a = s.nextDouble();
            if (a > 10.75) {
                b = b + a;
            }
        }
        System.out.println(b);
    }
}
