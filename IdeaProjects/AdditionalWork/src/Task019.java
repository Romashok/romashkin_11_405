import java.util.Random;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task019 {
    public static void main(String[] args) {
        Random r = new Random();
        int x = r.nextInt(2015);
        if (x % 100 == 0) {
            if (x % 400 == 0)
                System.out.println(x + "-високосный год");
            else
                System.out.println(x + "-не високосный год");

        } else if (x % 4 == 0)
            System.out.println(x + "-високосный год");
        else
            System.out.println(x + "-не високосный год");
    }


}
