import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task011 {
    public static void main(String[] args) {
        Scanner r = new Scanner(System.in);
        int n = r.nextInt(), m = r.nextInt();
        if (n % m == 0)
            System.out.println(n / m);
        else
            System.out.println("n на m нацело не делится");
    }


}
