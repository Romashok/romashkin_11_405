import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task018 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите двухзначное число:");
        int x = s.nextInt();
        if (x / 10 == 3 | x % 10 == 3)
            System.out.println("В числе " + x + " есть тройка");
        if (x / 10 == 6 | x % 10 == 6)
            System.out.println("В числе " + x + " есть шестерка");
        if (x / 10 == 9 | x % 10 == 9)
            System.out.println("В числе " + x + " есть девятка");
    }
}
