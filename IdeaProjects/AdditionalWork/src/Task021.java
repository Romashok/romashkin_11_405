import java.util.Scanner;

/**
 * Created by Romash on 16.11.2014.
 */
public class Task021 {
    public static void main(String[] args) {
        Scanner s= new Scanner(System.in);
        System.out.println("У команды очков:");
        int x=s.nextInt();
        if (x==3)
            System.out.println("команда выиграла");
        else if (x==1)
            System.out.println("команда сыграла в ничью");
        else
            System.out.println("команда проиграла");
    }
}
