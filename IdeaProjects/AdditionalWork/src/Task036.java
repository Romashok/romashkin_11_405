import java.util.Random;

/**
 * Created by Romash on 22.11.2014.
 */
public class Task036 {
    public static void main(String[] args) {
        Random r = new Random();
        int pos = r.nextInt(100);
        for (int i = 1; i < pos; i++) {
            System.out.print(i + ",");
        }
        System.out.println(pos);
    }
}
