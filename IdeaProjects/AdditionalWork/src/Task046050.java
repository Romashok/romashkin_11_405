import java.util.Random;

/**
 * Created by Romash on 22.11.2014.
 */
public class Task046050 {
    public static void main(String[] args) {
        Random r = new Random();
        int sum = 0, a = 0, n = r.nextInt(100);
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                a++;
                sum = sum + i;
            }
        }
        System.out.println(a + "\n" + sum);
    }
}
