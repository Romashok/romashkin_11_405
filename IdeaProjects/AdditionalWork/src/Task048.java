/**
 * Created by Romash on 22.11.2014.
 */
public class Task048 {
    public static void main(String[] args) {
        int a = 0, n = 300;
        for (int i = 1; i <= 300; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    a++;
                }
            }
            if (a == 5) {
                System.out.println(i);
            }
            a = 0;
        }
    }
}
