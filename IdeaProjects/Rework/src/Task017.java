import java.util.Scanner;

/**
 * Created by Romash on 16.11.14.
 */
public class Task017 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int l = 5, t;
        int[] a = new int[l];
        for (int i = 0; i < l; i++) {
            a[i] = s.nextInt();
        }
        for (int i = l - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a[j] > a[j + 1]) {
                    t = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = t;
                }
            }
        }
        for (int i = 0; i < l; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
