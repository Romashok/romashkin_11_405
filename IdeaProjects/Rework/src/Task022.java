import java.util.Scanner;

/**
 * Created by Romash on 16.11.14.
 */
public class Task022 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int m = 3, n = 3, sum;
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            sum = 0;
            for (int j = 0; j < n; j++) {
                a[i][j] = s.nextInt();
                for (int i1 = 0; i1 < n - 1; i1++) {
                    sum = sum + a[i][i1];
                }
            }
            a[i][n - 1] = sum;
        }
        for (int i = 0; i < m; i++) {
            sum = 0;
            for (int j = 0; j < n - 1; j++) {
                sum = sum + a[i][j];
            }
            a[i][n - 1] = sum;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
