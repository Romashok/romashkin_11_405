import java.util.Random;

/**
 * Created by Romash on 16.11.14.
 */
public class Task019 {
    public static void main(String[] args) {
        Random r = new Random();
        int l = r.nextInt(100);
        int[][] a = new int[l][l];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                a[i][j] = 0;
            }
        }
        for (int i = 0; i < l; i++) {
            a[i][i] = 1;
            a[i][l - 1 - i] = 1;
        }
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                System.out.print(a[i][j]);
            }
            System.out.println();
        }
    }
}
