/**
 * Created by Romash on 26.12.2014.
 */
public class fabonach {
    private static int fb(int n) {
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        } else return fb(n - 1) + fb(n - 2);

    }

    public static void main(String[] args) {
        int n = 8;
        System.out.println(fb(n));
    }

}