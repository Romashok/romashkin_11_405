import java.util.Scanner;

/**
 * Created by Romash on 25.12.2014.
 */
public class matrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] nod = new int[2];
        int[][] a = new int[2][n];
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < n; j++)
                a[i][j] = scanner.nextInt();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n - 1; j++) {
                int x = a[i][j];
                int s = a[i][j + 1];
                while (x != 0 && s != 0) {
                    if (x > s)
                        x %= s;
                    else
                        s %= x;
                }
                nod[i] = (x != 0) ? x : s;
            }
        }
        for (int i = 0; i < n; i++)
            System.out.println(nod[i]);
    }
}
