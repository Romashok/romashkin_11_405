/**
 * Created by Romash on 05.02.2015.
 */
public class LampMain {
    public static void main(String[] args) {
        Lamp lamp = new Lamp();
        lamp.status();
        lamp.changePos();
        lamp.status();
        for (int i = 0; i < 6; i++) {
            lamp.minusBright();
            lamp.status();
        }
        lamp.plusBright();
        lamp.changePos();
        for (int i = 0; i < 6; i++) {
            lamp.plusBright();
            lamp.status();
        }
    }
}
