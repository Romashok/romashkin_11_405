/**
 * Created by Romash on 17.01.2015.
 */
public class Task04 {
    public static void main(String[] args) {
        int a = 4, b = 33, c = 5, max, m, min;
        if (a < b & b < c) {
            System.out.println(a + " " + b + " " + c + "\n" + c + " " + b + " " + a);
        } else if (c < a & a < b) {
            System.out.println(c + " " + a + " " + b + "\n" + b + " " + a + " " + c);
        } else if (b < a & a < c) {
            System.out.println(b + " " + a + " " + c + "\n" + c + " " + a + " " + b);
        } else if (a < c & c < b) {
            System.out.println(a + " " + c + " " + b + "\n" + b + " " + c + " " + a);
        } else if (b < c & c < a) {
            System.out.println(b + " " + c + " " + a + "\n" + a + " " + c + " " + b);
        } else {
            System.out.println(c + " " + b + " " + a + "\n" + a + " " + b + " " + c);
        }
    }
}
