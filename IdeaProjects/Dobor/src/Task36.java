import java.util.Scanner;

/**
 * Created by Romash on 29.01.2015.
 */
public class Task36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine(), substr = sc.nextLine(), strtek;
        for (int i = 0; i <= str.length() - substr.length(); i++) {
            strtek = str.substring(i, i + substr.length());
            if (strtek.equals(substr)) {
                str = str.substring(0, i) + str.substring(i + substr.length(), str.length());
            }
        }
        System.out.println(str);
    }

}
