/**
 * Created by Romash on 05.02.2015.
 */
public class Lamp {
    private int bright;
    private boolean position = false;

    public void changePos() {
        if (!position) {
            bright = 5;
            position = true;
        } else {
            bright = 0;
            position = false;
        }
    }

    public void plusBright() {
        if (!position) {
            System.out.println("Лампа выключена! Яркость прибавить нельзя!");
        } else {
            if (bright != 10) {
                bright++;
            } else {
                System.out.println("Яркость уже на максимуме!");
            }
        }
    }

    public void minusBright() {
        if (bright != 0) {
            bright--;
            if (bright == 0) {
                position = false;
            }
        }else {
            System.out.println("Лампа выключена! Нельзя убавить яркость!");
        }
    }
    public void status(){
        System.out.println(position+" "+bright);
    }

    {

    }
}
