import java.util.Scanner;

/**
 * Created by Romash on 28.01.2015.
 */
public class Task21 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int l = 4;
        boolean b = true;
        int[][] a = new int[l][l];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                a[i][j] = s.nextInt();
            }
        }
        int si = 0, sin = 0, sj = 0, sjn = 0, sd1 = 0, sd2 = 0;
        for (int x = 0; x < l; x++) {
            sj = sj + a[0][x];
            si = si + a[x][0];
            sd1 = sd1 + a[x][x];
            sd2 = sd2 + a[x][l - 1 - x];
        }
        if (sd1 != sd2) {
            b = false;
        }
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                sjn = sjn + a[i][j];
                sin = sin + a[j][i];
            }
            if ((sjn != sj) || (sin != si)) {
                b = false;
                break;
            }
            sin = 0;
            sjn = 0;
        }
        if (b) {
            System.out.println("является");
        } else System.out.println("не является");
    }
}
