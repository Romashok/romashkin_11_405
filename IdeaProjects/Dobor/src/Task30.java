import java.util.Random;

/**
 * Created by Romash on 29.01.2015.
 */
public class Task30 {
    public static void main(String[] args) {
        Random r = new Random();
        int op = 0, a[][] = new int[3][3];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = r.nextInt(10);
            }
        }
        op = a[0][0] * a[1][1] * a[2][2] + a[1][0] * a[2][1] * a[0][2] + a[0][1] * a[1][2] * a[2][0] - a[0][2] * a[1][1] * a[2][0] - a[1][2] * a[2][1] * a[0][0] - a[2][1] * a[1][0] * a[2][2];
        System.out.println(op);
    }
}
