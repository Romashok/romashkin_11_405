import java.util.Scanner;

/**
 * Created by Romash on 28.01.2015.
 */
public class Task26 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = 3, m = 4, p = 0, ko = 0, a[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = s.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (a[i][j] == 0) {
                    p = 1;
                }
            }
            if (p == 1) {
                for (int j = 0; j < m; j++) {
                    if (a[i][j] < 0) {
                        ko++;
                    }
                }
                System.out.println("В " + i + " строчке " + ko + " отрицательных элементов");
            }
            ko = 0;
        }
    }
}
