import java.util.Random;

/**
 * Created by Romash on 29.01.2015.
 */
public class Task31 {
    public static void main(String[] args) {
        Random r = new Random();
        int zn = 0, n = r.nextInt(10), m = r.nextInt(10), a[] = new int[n], b[][] = new int[m][n], c[] = new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                b[i][j] = r.nextInt(10);
            }
            a[i] = r.nextInt(10);
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                c[i] = c[i] + a[j] * b[i][j];
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("\n");

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        for (int i = 0; i < m; i++) {
            System.out.print(c[i] + " ");
        }
    }
}
