import java.util.Scanner;

/**
 * Created by Romash on 03.02.2015.
 */
public class Task41 {
    public static boolean isParity(int a) {
        int sum = 0;
        while (a > 9) {
            sum = sum + a % 10;
            a = a / 10;
        }
        sum = sum + a;
        boolean b = false;
        if (sum % 2 == 0) {
            b = true;
        }
        return b;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean b = isParity(sc.nextInt());
        if (b) {
            System.out.println("четная");
        } else {
            System.out.println("нечетная");
        }
    }
}
