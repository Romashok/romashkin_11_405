import java.util.Scanner;

/**
 * Created by Romash on 03.02.2015.
 */
public class Task40 {
    public static int firstNumber(int a) {
        int min = a % 10;
        while (a >= 10) {
            a = a / 10;
            if (a % 10 < min) {
                min = a % 10;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        Scanner r = new Scanner(System.in);
        int result = firstNumber(r.nextInt());
        System.out.println(result);
    }
}
