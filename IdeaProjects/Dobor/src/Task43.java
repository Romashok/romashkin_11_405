import java.util.Scanner;

/**
 * Created by Romash on 03.02.2015.
 */
public class Task43 {
    public static char firstCharacter(String str) {
        char mas[] = str.toCharArray(), min = mas[0];
        for (int i = 1; i < str.length(); i++) {
            if (mas[i] < min) {
                min = mas[i];
            }
        }
        return min;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char ch = firstCharacter(sc.nextLine());
        System.out.println(ch);
    }
}
