/**
 * Created by Romash on 17.01.2015.
 */
public class Task03 {
    public static void main(String[] args) {
        int n = 2875, m = 0;
        while (n >= 10) {
            m = m + n % 10;
            n = n / 10;
        }
        m = m + n;
        System.out.println(m);
    }
}
