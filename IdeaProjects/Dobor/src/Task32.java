import java.util.Random;

/**
 * Created by Romash on 29.01.2015.
 */
public class Task32 {
    public static void main(String[] args) {
        Random r = new Random();
        double a[][] = new double[2][2], minor[][] = new double[2][2], op = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                a[i][j] = r.nextDouble();
            }
        }
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(a[i][j] + "  ");
            }
            System.out.println();
        }
        op = a[0][0] * a[1][1] - a[1][0] * a[0][1];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (i != j) {
                    minor[i][j] = -a[i][j] * (1 / op);
                } else {
                    minor[i][j] = a[j][i] * (1 / op);
                }
            }
        }
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                System.out.print(minor[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
