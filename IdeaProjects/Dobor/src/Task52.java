import java.util.Scanner;

/**
 * Created by Romash on 04.02.2015.
 */
public class Task52 {
    public static int findMembOfProg(int step, int lstnumb,int n){
        while (n>0){
            lstnumb=findMembOfProg(step,lstnumb+step,n-1);
            return lstnumb;

        }
        return lstnumb;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int step=sc.nextInt(),num=sc.nextInt(),n=sc.nextInt();
        System.out.println(findMembOfProg(step,num,n));
    }
}
