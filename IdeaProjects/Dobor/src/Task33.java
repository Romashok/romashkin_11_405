import java.util.Scanner;

/**
 * Created by Romash on 07.02.2015.
 */
public class Task33 {
    public static void main(String[] args) {
        int hor = 0, vert = 0, n = 0, m = 0;
        char doska[][] = new char[8][8];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                doska[i][j] = '0';
            }
        }
        System.out.println("Введите координаты для положения ладьи. От 0 до 7. Сначала горизонталь");
        hor = sc.nextInt();
        vert = sc.nextInt();
        doska[hor][vert] = 'L';
        while (n < doska.length) {
            if (doska[hor][n] != doska[hor][vert]) {
                doska[hor][n] = '*';
            }
            n++;
        }
        n = 0;
        while (n < doska.length) {
            if (doska[n][vert] != doska[hor][vert]) {
                doska[n][vert] = '*';
            }
            n++;
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                System.out.print(doska[i][j] + "  ");
            }
            System.out.println();
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                doska[i][j] = '0';
            }
        }
        System.out.println("Введите координаты для положения слона. От 0 до 7. Сначала горизонталь");
        hor = sc.nextInt();
        vert = sc.nextInt();
        doska[hor][vert] = 'S';
        n = 1;
        m = 1;
        while (hor - n >= 0 & vert - m >= 0) {
            doska[hor - n][vert - m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor + n < doska.length & vert + m < doska.length) {
            doska[hor + n][vert + m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor - n >= 0 & vert + m < doska.length) {
            doska[hor - n][vert + m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor + n < doska.length & vert - m >= 0) {
            doska[hor + n][vert - m] = '*';
            n++;
            m++;
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                System.out.print(doska[i][j] + "  ");
            }
            System.out.println();
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                doska[i][j] = '0';
            }
        }
        System.out.println("Введите координаты для положения ферзя. От 0 до 7. Сначала горизонталь");
        hor = sc.nextInt();
        vert = sc.nextInt();
        doska[hor][vert] = 'F';
        n = 0;
        while (n < doska.length) {
            if (doska[hor][n] != doska[hor][vert]) {
                doska[hor][n] = '*';
            }
            n++;
        }
        n = 0;
        while (n < doska.length) {
            if (doska[n][vert] != doska[hor][vert]) {
                doska[n][vert] = '*';
            }
            n++;
        }
        n = 1;
        m = 1;
        while (hor - n >= 0 & vert - m >= 0) {
            doska[hor - n][vert - m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor + n < doska.length & vert + m < doska.length) {
            doska[hor + n][vert + m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor - n >= 0 & vert + m < doska.length) {
            doska[hor - n][vert + m] = '*';
            n++;
            m++;
        }
        n = 1;
        m = 1;
        while (hor + n < doska.length & vert - m >= 0) {
            doska[hor + n][vert - m] = '*';
            n++;
            m++;
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                System.out.print(doska[i][j] + "  ");
            }
            System.out.println();
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                doska[i][j] = '0';
            }
        }
        System.out.println("Введите координаты для положения коня. От 0 до 7. Сначала горизонталь");
        hor = sc.nextInt();
        vert = sc.nextInt();
        doska[hor][vert] = 'K';
        if (hor + 2 < doska.length & vert + 1 < doska.length) {
            doska[hor + 2][vert + 1] = '*';
        }
        if (hor + 1 < doska.length & vert + 2 < doska.length) {
            doska[hor + 1][vert + 2] = '*';
        }
        if (hor - 1 >= 0 & vert - 2 >= 0) {
            doska[hor - 1][vert - 2] = '*';
        }
        if (hor - 2 >= 0 & vert - 1 >= 0) {
            doska[hor - 2][vert - 1] = '*';
        }
        if (hor - 2 >= 0 & vert + 1 < doska.length) {
            doska[hor - 2][vert + 1] = '*';
        }
        if (hor - 1 >= 0 & vert + 2 < doska.length) {
            doska[hor - 1][vert + 2] = '*';
        }
        if (hor + 1 < doska.length & vert - 2 >= 0) {
            doska[hor + 1][vert - 2] = '*';
        }
        if (hor + 2 < doska.length & vert - 1 >= 0) {
            doska[hor + 2][vert - 1] = '*';
        }
        for (int i = 0; i < doska.length; i++) {
            for (int j = 0; j < doska.length; j++) {
                System.out.print(doska[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
