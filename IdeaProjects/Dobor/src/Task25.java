import java.util.Scanner;

/**
 * Created by Romash on 28.01.2015.
 */
public class Task25 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int p = 1, n = s.nextInt();
        int a[][] = new int[n][n];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = s.nextInt();
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (a[i][j] != a[j][i]) {
                    p = 0;
                }
            }
        }
        if (p == 1) {
            System.out.println("Симметричен");
        } else {
            System.out.println("Несимметричен");
        }
    }
}
