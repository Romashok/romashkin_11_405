import java.util.Scanner;

/**
 * Created by Romash on 04.02.2015.
 */
public class Task49 {
    public static int Nod(int a, int b) {
        int nod = 0;
        if (a <= b) {
            for (int i = 1; i <= a / 2; i++) {
                if (a % i == 0) {
                    if (b % (a / i) == 0) {
                        nod =a / i;
                        i=a/2;
                    }
                }
            }
        } else {
            for (int i = 1; i <= b / 2; i++) {
                if (b % i == 0) {
                    if (a % (b / i) == 0) {
                        nod =b / i;
                        i=a/2;
                    }
                }
            }
        }
        return nod;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt(), b = sc.nextInt(), c = sc.nextInt(), d = 0, nod = 0;
        d = Nod(a, b);
        nod = Nod(c, d);
        System.out.println(nod);
    }
}
