import java.util.Scanner;

/**
 * Created by Romash on 04.02.2015.
 */
public class Task48 {
    public static double bConcetration(String str1, String str2) {
        double concstr1, concstr2,bstr1 = 0, bstr2 = 0;
        char[] mas1 = str1.toCharArray(), mas2 = str2.toCharArray();
        for (int i = 0; i < str1.length(); i++) {
            if (mas1[i] == 'б') {
                bstr1++;
            }
        }
        for (int i = 0; i < str2.length(); i++) {
            if (mas2[i] == 'б') {
                bstr2++;
            }
        }
        concstr1 =  (bstr1 / str1.length()) * 100;
        concstr2 =  (bstr2 / str2.length()) * 100;
        if (concstr1 >= concstr2) {
            return concstr1;
        } else {
            return concstr2;
        }
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double conc = bConcetration(sc.nextLine(), sc.nextLine());
        System.out.println(conc + "%");
    }
}