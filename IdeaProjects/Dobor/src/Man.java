/**
 * Created by Romash on 05.02.2015.
 */
public class Man {
    private String name;
    private int age;
    private int weight;
    private String sex;
    public Man(String name){
        this.name=name;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public void setWeight(int weight){
        this.weight=weight;
    }
    public void setSex(String sex){
        this.sex=sex;
    }
    public void status(){
        System.out.println("Имя:"+name+"\n"+"Возраст:"+age+"\n"+"Вес:"+weight+"\n"+"Пол:"+sex);
    }
}
