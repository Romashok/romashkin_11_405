import java.util.Scanner;

/**
 * Created by Romash on 03.02.2015.
 */
public class Task45 {
    public static String firstString(String[] strmas) {
        String str = strmas[0];
        for (int j = 1; j < strmas.length; j++) {

            char[] mas1 = strmas[j].toCharArray(), mas2 = str.toCharArray();
            if (strmas[j].length() <= str.length()) {
                for (int i = 0; i < strmas[j].length(); i++) {
                    if (mas1[i] != mas2[i]) {
                        if (mas1[i] < mas2[i]) {
                            str = strmas[j];
                        }
                    }
                }
            } else {
                for (int i = 0; i < str.length(); i++) {
                    if (mas1[i] != mas2[i]) {
                        if (mas1[i] < mas2[i]) {
                            str = strmas[j];
                        }
                    }
                }

            }

        }
        return str;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strmas = new String[5];
        for (int i = 0; i < strmas.length; i++) {
            strmas[i] = sc.nextLine();
        }
        String str = firstString(strmas);
        System.out.println(str);
    }
}
