import java.util.Scanner;

/**
 * Created by Romash on 28.01.2015.
 */
public class Task20 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int l = 3;
        int[][] a = new int[l][l];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                a[i][j] = s.nextInt();
            }
        }
        int max1 = a[0][1], max2 = a[1][0];

        for (int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {
                if (j > i) {
                    if (max1 < a[i][j]) {
                        max1 = a[i][j];
                    }
                } else {
                    if (j != i) {
                        if (max2 < a[i][j]) {
                            max2 = a[i][j];
                        }
                    }
                }
            }
        }
        System.out.println("Максимум снизу: " + max2 + " Максимум сверху: " + max1);
    }
}
