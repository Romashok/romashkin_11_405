import java.util.Random;

/**
 * Created by Romash on 21.01.2015.
 */
public class Task17 {
    public static void main(String[] args) {
        Random r = new Random();
        int m = 0, b = r.nextInt(10), a[] = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = r.nextInt(10);
            System.out.println(a[i]);
        }
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    m = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = m;
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
}
