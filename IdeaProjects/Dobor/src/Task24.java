import java.util.Random;

/**
 * Created by Romash on 28.01.2015.
 */
public class Task24 {
    public static void main(String[] args) {
        Random rand = new Random();
        int n = 0;
        while (n == 0 | n % 2 == 0) {
            n = rand.nextInt(20);
        }
        int mas[][] = new int[n][n], min_main = 0, min_second = 0, i1 = 0, i2 = 0, j1 = 0, j2 = 0, g = 0;
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                mas[i][j] = rand.nextInt(100);
            }
        }
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                min_main = mas[i][i];
                i1 = i;
                j1 = i;
            }
            if (min_main > mas[i][i]) {
                min_main = mas[i][i];
                i1 = i;
                j1 = i;
            }
        }
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                min_second = mas[i][n - i - 1];
                i2 = i;
                j2 = n - 1 - i;
            }
            if (min_second > mas[i][n - i - 1]) {
                min_second = mas[i][n - i - 1];
                i2 = i;
                j2 = n - 1 - i;
            }
        }
        if (min_main <= min_second) {
            g = mas[n - 1][0];
            mas[n - 1][0] = mas[i1][j1];
            mas[i1][j1] = g;
        } else {
            g = mas[n - 1][0];
            mas[n - 1][0] = mas[i2][j2];
            mas[i2][j2] = g;
        }
        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas.length; j++) {
                System.out.print(mas[i][j] + " ");
            }
            System.out.println();
        }

    }
}
