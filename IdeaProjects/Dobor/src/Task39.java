import java.util.Random;

/**
 * Created by Romash on 03.02.2015.
 */
public class Task39 {
    public static double solveEquation(int a, int b) {
        double x = (double) a / b;
        return x;
    }

    public static void main(String[] args) {
        Random r = new Random();

        int a = r.nextInt(10), b = r.nextInt(10);
        System.out.println(a+" "+b);
        double x = solveEquation(a, b);
        System.out.println(x);
    }
}
