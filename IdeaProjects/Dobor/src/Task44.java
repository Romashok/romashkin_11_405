import java.util.Scanner;

/**
 * Created by Romash on 04.02.2015.
 */
public class Task44 {
    public static String firstString(String str1, String str2) {
        String str = new String();
        char[] mas1 = str1.toCharArray(), mas2 = str2.toCharArray();
        if (str1.length() <= str2.length()) {
            for (int i = 0; i < str1.length(); i++) {
                if (mas1[i] != mas2[i]) {
                    if (mas1[i] < mas2[i]) {
                        str = str1;
                        break;
                    } else {
                        str = str2;
                        break;
                    }
                }
            }
        } else {
            for (int i = 0; i < str2.length(); i++) {
                if (mas1[i] != mas2[i]) {
                    if (mas1[i] < mas2[i]) {
                        str = str1;
                        break;
                    } else {
                        str = str2;
                        break;
                    }
                }
            }

        }
        return str;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = firstString(sc.nextLine(), sc.nextLine());
        System.out.println(str);
    }
}
