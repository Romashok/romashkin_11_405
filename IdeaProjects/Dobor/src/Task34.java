import java.util.Random;


/**
 * Created by Romash on 29.01.2015.
 */
public class Task34 {
    public static void main(String[] args) {
        Random r = new Random();

        int ch = r.nextInt(1), n = r.nextInt(10), m = r.nextInt(20), a[][] = new int[n][n], b[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = r.nextInt(100);
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println(m);
        if (m > n) {
            while (m >= n) {
                m = m - n;
            }
        }
        if (ch == 1) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (j + m >= n) {
                        b[i][j] = a[i][j + m - n];
                    } else {
                        b[i][j] = a[i][j + n];
                    }
                }
            }
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i + m >= n) {
                        b[i][j] = a[i + m - n][j];
                    } else {
                        b[i][j] = a[i + m][j];
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(b[i][j] + "  ");
            }
            System.out.println();
        }
    }
}


