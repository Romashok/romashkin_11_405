/**
 * Created by Romash on 03.02.2015.
 */
public class Task42 {
    public static boolean isRight(boolean x, boolean y, boolean z) {
        boolean a = false;
        if ((x && !y) || (!x && y) && z) {
            a = true;
        }
        return a;
    }

    public static void main(String[] args) {
        boolean a = isRight(true, false, true);
        System.out.println(a);
    }
}
