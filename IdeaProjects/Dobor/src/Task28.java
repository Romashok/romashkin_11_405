import java.util.Random;

/**
 * Created by Romash on 29.01.2015.
 */
public class Task28 {
    public static void main(String[] args) {
        Random r = new Random();
        int n = r.nextInt(10), a[][] = new int[n][n + 2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                a[i][j + i] = 1;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n + 2; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
