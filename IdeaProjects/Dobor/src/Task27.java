import java.util.Random;


/**
 * Created by Romash on 29.01.2015.
 */
public class Task27 {
    public static void main(String[] args) {
        Random s = new Random();
        int n = s.nextInt(10), a[][] = new int[n][n];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = s.nextInt(100);
                a[j][i] = a[i][j];
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
