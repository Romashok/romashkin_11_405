import java.util.Random;

/**
 * Created by Romash on 18.01.2015.
 */
public class Task13 {
    public static void main(String[] args) {
        Random r = new Random();
        int k = 0, a[] = new int[35], max = r.nextInt(200);
        for (int i = 0; i < a.length; i++) {
            while (a[i] < 70) {
                a[i] = r.nextInt(200);
            }
            if (max < a[i]) {
                max = a[i];
            }
            if (max == a[i]) {
                k = k + 1;
            }
        }
        System.out.println("Самый высокий: " + max + "\n" + "Таких человек: " + k);
    }
}
