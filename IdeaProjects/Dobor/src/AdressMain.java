import java.util.Scanner;

/**
 * Created by Romash on 06.02.2015.
 */
public class AdressMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Adress a = new Adress(sc.nextLine(), sc.nextLine());
        a.setIndex();
        a.showIndex();
        a.showCountry();
    }
}
