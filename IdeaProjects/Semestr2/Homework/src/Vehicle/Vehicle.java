package Vehicle;

/**
 * Created by Romash on 01.03.2015.
 */
public abstract class Vehicle {
    abstract void drive();
}
