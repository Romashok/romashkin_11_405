package Vehicle;

/**
 * Created by Romash on 01.03.2015.
 */
public class Main {
    public static void main(String[] args) {
        Vehicle[] vehicles = new Vehicle[50];
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = new Tram();
            vehicles[i].drive();
        }
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = new Bike();
            vehicles[i].drive();
        }
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = new Bus();
            vehicles[i].drive();
        }
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = new Car();
            vehicles[i].drive();
        }
        for (int i = 0; i < vehicles.length; i++) {
            vehicles[i] = new Motorcycle();
            vehicles[i].drive();
        }
    }
}
