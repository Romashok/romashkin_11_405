package Transport;

/**
 * Created by Romash on 25.02.2015.
 */
public class Bus extends Transport {
    int vol=10;
    public void PlusPeople(int a){

        if (vol-a>=0){
            vol+=-a;
            System.out.println("Люди зашли. Свободных мест:"+vol);
        }else {
            System.out.println("Мест на всех не хватит. А это очень близкие люди, и разделяться они не хотят. Так что никто не зашел");
        }

    }
    public void MinusPeople(int a){
        if (vol+a<=10){
            vol+=a;
            System.out.println("Люди вышли. Свободных мест:"+vol);
        }else {
            vol=10;
            System.out.println("К сожалению число желающих выйти превышает число занятых мест. Поэтому вышли только некоторые из желающих.");
        }
    }
}
