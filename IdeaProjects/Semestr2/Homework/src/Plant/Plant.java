package Plant;

/**
 * Created by Romash on 01.03.2015.
 */
public abstract class Plant {
    public void grow() {
        System.out.println("Plant growing");
    }

    public void water() {
        System.out.println("Plant watered");
    }

    public void fertilize() {
        System.out.println("Plant fertilized");
    }

    abstract void fruit();

}

