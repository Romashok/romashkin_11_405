package Plant;

/**
 * Created by Romash on 01.03.2015.
 */
public class Apple extends Plant {
    @Override
    void fruit() {
        System.out.println("Apple ripe");
    }
}
