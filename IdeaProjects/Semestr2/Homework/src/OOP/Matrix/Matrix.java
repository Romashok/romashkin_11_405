package Matrix;

/**
 * Created by Romash on 14.02.2015.
 */
public class Matrix {
    public double matr[][];
    public int str_stl;


    public void setMatr(double matr[][]) {
        this.matr = matr;
        str_stl=matr.length;
    }



    public void plusMatr(double matr1[][]) {
        if (str_stl != matr1.length) {
            System.out.println("Матрицы сложить нельзя");
        } else {
            for (int i = 0; i < str_stl; i++) {
                for (int j = 0; j < str_stl; j++) {
                    matr[i][j] = matr[i][j] + matr1[i][j];
                }
            }
        }
    }

    public void mult_dig(int a) {
        for (int i = 0; i < str_stl; i++) {
            for (int j = 0; j < str_stl; j++) {
                matr[i][j] = matr[i][j] * a;
            }
        }
    }

    public void mult_matr(double matr1[][]) {
        double matr_ans[][] = new double[str_stl][str_stl];
        double a = 0;
        if (str_stl != matr1.length) {
            System.out.println("Матрицы нельзя перемножить");
        } else {
            for (int i = 0; i < str_stl; i++) {
                for (int j = 0; j < str_stl; j++) {
                    for (int k = 0; k < str_stl; k++) {
                        a = a + matr[i][k] * matr1[k][j];
                    }
                    matr_ans[i][j] = a;
                    a = 0;
                }
                a = 0;
            }
        }
        for (int i = 0; i < str_stl; i++) {
            for (int j = 0; j < str_stl; j++) {
                matr[i][j] = matr_ans[i][j];
            }
        }
    }

    public void trans() {
        for (int i = 0; i < str_stl; i++) {
            for (int j = 0; j < str_stl; j++) {
                matr[i][j] = matr[j][i];
            }
        }
    }

    public void showMatr() {
        for (int i = 0; i < str_stl; i++) {
            for (int j = 0; j < str_stl; j++) {
                System.out.print(matr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
