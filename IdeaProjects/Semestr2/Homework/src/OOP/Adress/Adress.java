package Adress;

/**
 * Created by Romash on 13.02.2015.
 */
public class Adress {
    public String country;
    public String region;
    public String city;
    public String street;
    public String number;
    public int index;

    public void setCountry() {
        this.country = country;
    }

    public void setRegion() {
        this.region = region;
    }

    public void setCity() {
        this.city = city;
    }

    public void setStreet() {
        this.street = street;
    }

    public void setNumber() {
        this.number = number;
    }

    public void setIndex() {
        this.index = index;
    }

    public Adress(String country) {
        this.country = country;
    }

    public Adress(String country, String region) {
        this.country = country;
        this.region = region;
    }

    public Adress(String country, String region, String city) {
        this.country = country;
        this.region = region;
        this.city = city;
    }

    public Adress(String country, String region, String city, String street) {
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
    }

    public Adress(String country, String region, String city, String street, String number) {
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.number = number;
    }

    public Adress(String country, String region, String city, String street, String number, int index) {
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.number = number;
        this.index = index;
    }

    public void showCountry() {
        System.out.println(country);
    }

    public void showRegion() {
        System.out.println(region);
    }

    public void showCity() {
        System.out.println(city);
    }

    public void showStreet() {
        System.out.println(street);
    }

    public void showNumber() {
        System.out.println(number);
    }

    public void showIndex() {
        System.out.println(index);
    }
}
