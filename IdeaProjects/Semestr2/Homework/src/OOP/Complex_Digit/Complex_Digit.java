package Complex_Digit;

/**
 * Created by Romash on 13.02.2015.
 */
public class Complex_Digit {
    private int re;
    private int im;

    public Complex_Digit(int re, int im) {
        this.re = re;
        this.im = im;
    }

    public void plus_dig(int re1, int im1) {
        int re_res = re + re1;
        int im_res = im + im1;
        System.out.println(re_res + "+" + im_res + "i");
    }

    public void mult_dig(int re1, int im1) {
        int re_res = re * re1 - (im * im1);
        int im_res = re * im1 + re1 * im;
        System.out.println(re_res + "+" + im_res + "i");
    }

    public void mod() {
        double mod = Math.sqrt(re * re + im * im);
        System.out.println('|' + mod + '|');
    }
}
