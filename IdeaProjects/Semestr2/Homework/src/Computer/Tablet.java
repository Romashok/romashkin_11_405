package Computer;

/**
 * Created by Romash on 14.02.2015.
 */
public class Tablet extends Laptop {
    String os;
    Monitor m = new Monitor(rand.nextInt(100), rand.nextInt(150), rand.nextInt(80), rand.nextInt(3000));

    public Tablet(int frequency, int num_of_cores, int ram, String videocard, int hdd, int battery, String os) {
        super(frequency, num_of_cores, ram, videocard, hdd, battery);
        this.os = os;
    }


}
