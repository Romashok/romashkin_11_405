package Computer;

import java.util.Random;


/**
 * Created by Romash on 14.02.2015.
 */
public class Laptop extends Computer {
    Random rand=new Random();
    int battery;
    Monitor monitor;
    public Laptop(Monitor monitor,int frequency, int num_of_cores, int ram, String videocard, int hdd, int battery) {
        super(frequency, num_of_cores, ram, videocard, hdd);
        this.battery=battery;

    }

    public void charge() {
        battery = 100;
    }

}
