package Computer;

/**
 * Created by Romash on 14.02.2015.
 */
public class Computer {
    private int frequency;
    private int num_of_cores;
    private int ram;
    private String videocard;
    private int hdd;

    public Computer(int frequency, int num_of_cores,int ram,String videocard,int hdd) {
        this.frequency=frequency;
        this.hdd=hdd;
        this.num_of_cores=num_of_cores;
        this.ram=ram;
        this.videocard=videocard;
    }

    public void setOn() {
        System.out.println("Hello Master!");
    }

    public void setOff() {
        System.out.println("Bye");
    }
}
