package Instrument;

/**
 * Created by Romash on 01.03.2015.
 */
public class Percussion extends Instrument {
    @Override
    public void GetName() {
        System.out.println("Percussion");
    }

    @Override
    public void Adjust() {
        System.out.println("Error");
    }

    @Override
    public void Play() {
        System.out.println("Percussion' music");
    }
}
