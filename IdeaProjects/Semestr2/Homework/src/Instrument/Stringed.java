package Instrument;

/**
 * Created by Romash on 01.03.2015.
 */
public class Stringed extends Instrument{
    @Override
    public void Play() {
        System.out.println("Stringed's music");
    }

    @Override
    public void GetName() {
        System.out.println("Stringed");
    }

    @Override
    public void Adjust() {
        System.out.println("Stringed set");
    }
}
