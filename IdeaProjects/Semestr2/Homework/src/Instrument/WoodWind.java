package Instrument;

/**
 * Created by Romash on 01.03.2015.
 */
public class WoodWind extends Wind {
    @Override
    public void Play() {
        System.out.println("woodwind's music");
    }

    @Override
    public void GetName() {
        System.out.println("WoodWind");
    }

    @Override
    public void Adjust() {
        System.out.println("Error");
    }
}
