package Instrument;

/**
 * Created by Romash on 01.03.2015.
 */
public class BrassWind extends Wind {
    @Override
    public void Play() {
        System.out.println("brasswind's music");
    }

    @Override
    public void GetName() {
        System.out.println("BrassWind");
    }

    @Override
    public void Adjust() {
        System.out.println("BrassWind set ");
    }
}
