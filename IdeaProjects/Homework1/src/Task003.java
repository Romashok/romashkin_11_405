/**
 * Created by Romash on 28.09.2014.
 */
public class Task003 {
    public static void main(String[] args) {
        int n = 2875, m = 0;
        while (n >= 10) {
            m = m + n % 10;
            n = n / 10;
        }
        m = m + n;
        System.out.println(m);
    }
}
