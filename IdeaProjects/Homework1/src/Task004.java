/**
 * Created by Romash on 28.09.2014.
 */
public class Task004 {
    public static void main(String[] args) {
        int a = 4, b = 33, c = 5, max, m, min;
        if (a < b && a < c)
            min = a;
        else if (c < a && c < b)
            min = c;
        else
            min = b;
        if (a > b && a > c)
            max = a;
        else if (c > a && c > b)
            max = c;
        else
            max = b;
        if ((a == max && c == min) || (a == min && c == max))
            m = b;
        else if ((a == max && b == min) || (a == min && b == max))
            m = c;
        else m = b;
        System.out.println(max + " " + m + " " + min);
        System.out.println(min + " " + m + " " + max);


    }
}
