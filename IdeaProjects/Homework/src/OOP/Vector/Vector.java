package Vector;

/**
 * Created by Romash on 13.02.2015.
 */
public class Vector {
    private int mas[];
    public void setMas(int[] mas){
        this.mas=mas;
    }

    public void cleanMas(){
        for (int i=0;i<mas.length;i++){
            mas[i]=0;
        }
    }
    public void setEl_I(int i,int n){
        if (i>=0&i<mas.length){
            mas[i]=n;
        }else {
            System.out.println("Недопустимое знaчение");
        }
    }
    public void delEl_I(int i){
        if (i>=0&i<mas.length){
            mas[i]=0;
        }else {
            System.out.println("Недопустимое знaчение");
        }
    }
    public void showMas(){
        for (int i=0;i<mas.length;i++){
            System.out.println(mas[i]);
        }
    }
}
