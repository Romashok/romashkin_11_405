package Matrix;

import java.util.Random;

/**
 * Created by Romash on 14.02.2015.
 */
public class main {
    public static void main(String[] args) {
        Random rand = new Random();
        int a = rand.nextInt(10);
        double matr[][] = new double[a][a];
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr.length; j++) {
                matr[i][j] = rand.nextInt(100);
            }
        }
        Matrix m = new Matrix();
        m.setMatr(matr);
        m.mult_dig(rand.nextInt(10));
        m.trans();
        System.out.println();
        m.showMatr();
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr.length; j++) {
                matr[i][j] = rand.nextInt(100);
            }
        }
        m.plusMatr(matr);
        System.out.println();
        m.showMatr();
        m.setMatr(matr);
        m.mult_matr(matr);
        System.out.println();
        m.showMatr();
    }
}
