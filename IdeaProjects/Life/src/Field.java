import java.util.Random;
import java.util.Scanner;

/**
 * Created by Romash on 29.03.2015.
 */
public class Field {
    int vert;
    int hor;
    char[][] field;
    Scanner sc=new Scanner(System.in);
    Random rand=new Random();
    int setVert(int vert) {
        this.vert = vert;
        if (vert >= 5) {
            return vert;
        } else {
            this.vert = vert + 5;
            System.out.println("Программа самостоятельно увеличила количество строк, так как вы ввели слишком маленькое.Теперь оно равно " + this.vert);
            return vert;
        }
    }
    int setHor(int hor) {
        this.hor = hor;
        if (hor >= 5) {
            return hor;
        } else {
            this.hor = hor + 5;
            System.out.println("Программа самостоятельно увеличила количество столбцов, так как вы ввели слишком маленькое. Теперь оно равно " + this.hor);
            return hor;
        }
    }
    public char[][] setField() {
        field = new char[vert][hor];
        return field;
    }
    public char[][] setSetField(int num, char[][] ms) {
        int a;
        int b;
        int c;
        System.out.println("Хорошо, теперь выберите режим: 1 - забить поле самому. 0 - позволить сделать это компуктеру. Размер вашего поля: " + ms.length + "x" + ms[0].length);
        a=sc.nextInt();
        while (a != 1 && a != 0) {
            System.out.println("Введите еще раз пожалуйста");
            a = sc.nextInt();
        }
        if (a==0) {
            for (int i = 0; i < num / 3; i++) {
                a = rand.nextInt(ms.length);
                b = rand.nextInt(ms[0].length);
                if (ms[a][b] == '_') {
                    ms[a][b] = 'X';
                } else {
                    while (ms[a][b] != '_') {
                        a = rand.nextInt(ms.length);
                        b = rand.nextInt(ms[0].length);
                    }
                    ms[a][b] = 'X';
                }
            }
        } else {
            System.out.println("Введите количество живых клеток от 1 до " + (num-1) + " Размер вашего поля: " + ms.length + "x" + ms[0].length);
            c = sc.nextInt();
            while (c < 1 || c > num-1) {
                System.out.println("Вы ввели неподходящее количество. Давайте ка еще раз.Размер вашего поля: " + ms.length + "x" + ms[0].length);
                c = sc.nextInt();
            }
            for (int i = 0; i < c; i++) {
                System.out.println("Выберите координаты для следующей точки. Сначала столбец. Размер вашего поля: " + ms.length + "x" + ms[0].length);
                a = sc.nextInt();
                b = sc.nextInt();
                while (a < 0 || b < 0 || a > ms.length - 1 || b > ms[0].length - 1) {
                    System.out.println("Вы ввели неподходящие координаты. Выберите координаты для следующей точки. Сначала столбец.Размер вашего поля: " + ms.length + "x" + ms[0].length);
                    a = sc.nextInt();
                    b = sc.nextInt();
                }
                while (ms[a][b] == 'X') {
                    System.out.println("Эта точка уже живая, введите другие координаты. Выберите координаты для следующей точки. Сначала столбец. Размер вашего поля: " + ms.length + "x" + ms[0].length);
                    a = sc.nextInt();
                    b = sc.nextInt();
                    while (a < 0 | b < 0 | a > ms.length - 1 | b > ms[0].length - 1) {
                        System.out.println("Вы ввели неподходящие координаты. Выберите координаты для следующей точки. Сначала столбец.Размер вашего поля: " + ms.length + "x" + ms[0].length);
                        a = sc.nextInt();
                        b = sc.nextInt();
                    }
                }
                ms[a][b] = 'X';

            }

        }
        return ms;
    }
    public char proverka(char[][] temp) {
        int a;
        if (temp[1][1] == '_') {
            a = 0;
            for (int k = 0; k < temp.length; k++) {
                for (int l = 0; l < temp[0].length; l++) {
                    if (temp[k][l] == 'X') {
                        a++;
                    }

                }
            }
            if (a == 3) {
                temp[1][1] = 'X';
            }
        } else if (temp[1][1] == 'X') {
            a = -1;
            for (int k = 0; k < temp.length; k++) {
                for (int l = 0; l < temp[0].length; l++) {
                    if (temp[k][l] == 'X') {
                        a++;
                    }
                }
            }
            if (a < 2 || a > 3) {
                temp[1][1] = '_';
            }
        }
        return temp[1][1];
    }
    public int proverka2(char[][]ms,char[][]ms1, int num,int c,int b){
        int result=0;
        for (int i = 0; i < ms.length; i++) {
            for (int j = 0; j < ms[0].length; j++) {
                if (ms1[i][j] != ms[i][j]) {
                    c = 1;
                }
                if (ms[i][j] == '_') {
                    num = 1;
                }
                if (ms[i][j] == 'X') {
                    b = 1;
                }
            }
        }
        if (c == 0 || num == 1 & b == 0) {
            result=1;
        }
        return result;
    }
}


