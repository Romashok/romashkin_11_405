import java.util.Scanner;

/**
 * Created by Romash on 29.03.2015.
 */
public class Main {
    public static void main(String[] args) {
        char[][] ms, ms1;
        char[][] temp = new char[3][3];
        int num = 0;
        int a = 1;
        int b;
        int c;
        Scanner sc = new Scanner(System.in);
        Field fd = new Field();
        System.out.println("Добро пожаловать в игру 'жизнь'! Пожалуйста введите размеры поля не меньше 5. Для начала количество строк.");
        fd.setVert(sc.nextInt());
        System.out.println("А теперь введите количество столбцов. Опять же, не меньше 5");
        fd.setHor(sc.nextInt());
        ms = fd.setField();
        ms1=fd.setField();
        for (int i = 0; i < ms.length; i++) {
            for (int j = 0; j < ms[0].length; j++) {
                ms[i][j] = '_';
                num++;
            }
        }
        ms = fd.setSetField(num, ms);
        System.out.println("Первое поколение");
        for (int i = 0; i < ms.length; i++) {
            for (int j = 0; j < ms[0].length; j++) {
                System.out.print(ms[i][j] + " ");
            }
            System.out.println();
        }
        for (int p = 0; p < 1000; p++) {
            for (int i = 0; i < ms.length; i++) {
                for (int j = 0; j < ms[0].length; j++) {
                    if (i == 0 && j == 0) {
                        temp[0][0] = '_';
                        temp[0][1] = '_';
                        temp[0][2] = '_';
                        temp[1][0] = '_';
                        temp[2][0] = '_';
                        temp[1][1] = ms[i][j];
                        temp[1][2] = ms[i][j + 1];
                        temp[2][1] = ms[i + 1][j];
                        temp[2][2] = ms[i + 1][j + 1];
                        ms[i][j] = fd.proverka(temp);

                    } else if (i == 0 && j == ms[0].length - 1) {
                        temp[0][0] = '_';
                        temp[0][1] = '_';
                        temp[0][2] = '_';
                        temp[1][2] = '_';
                        temp[2][2] = '_';
                        temp[1][1] = ms[i][j];
                        temp[1][0] = ms[i][j - 1];
                        temp[2][1] = ms[i + 1][j];
                        temp[2][0] = ms[i + 1][j - 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i == ms.length - 1 && j == 0) {
                        temp[0][0] = '_';
                        temp[2][1] = '_';
                        temp[2][2] = '_';
                        temp[1][0] = '_';
                        temp[2][0] = '_';
                        temp[1][1] = ms[i][j];
                        temp[0][1] = ms[i - 1][j];
                        temp[0][2] = ms[i - 1][j + 1];
                        temp[1][2] = ms[i][j + 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i == ms.length - 1 && j == ms[0].length - 1) {
                        temp[0][2] = '_';
                        temp[2][1] = '_';
                        temp[2][2] = '_';
                        temp[1][2] = '_';
                        temp[2][0] = '_';
                        temp[1][1] = ms[i][j];
                        temp[0][1] = ms[i - 1][j];
                        temp[0][0] = ms[i - 1][j - 1];
                        temp[1][0] = ms[i][j - 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i == 0 && j > 0 && j < ms[0].length - 1) {
                        temp[0][0] = '_';
                        temp[0][1] = '_';
                        temp[0][2] = '_';
                        temp[1][0] = ms[i][j - 1];
                        temp[2][0] = ms[i + 1][j - 1];
                        temp[1][1] = ms[i][j];
                        temp[2][1] = ms[i + 1][j];
                        temp[1][2] = ms[i][j + 1];
                        temp[2][2] = ms[i + 1][j + 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i == ms.length - 1 && j > 0 && j < ms[0].length - 1) {
                        temp[2][0] = '_';
                        temp[2][1] = '_';
                        temp[2][2] = '_';
                        temp[1][0] = ms[i][j - 1];
                        temp[0][0] = ms[i - 1][j - 1];
                        temp[1][1] = ms[i][j];
                        temp[0][1] = ms[i - 1][j];
                        temp[1][2] = ms[i][j + 1];
                        temp[0][2] = ms[i - 1][j + 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i > 0 && i < ms.length - 1 && j == 0) {
                        temp[0][0] = '_';
                        temp[1][0] = '_';
                        temp[2][0] = '_';
                        temp[0][2] = ms[i - 1][j + 1];
                        temp[0][1] = ms[i - 1][j];
                        temp[1][1] = ms[i][j];
                        temp[2][1] = ms[i + 1][j];
                        temp[1][2] = ms[i][j + 1];
                        temp[2][2] = ms[i + 1][j + 1];
                        ms[i][j] = fd.proverka(temp);
                    } else if (i > 0 && i < ms.length - 1 && j == ms[0].length - 1) {
                        temp[0][2] = '_';
                        temp[1][2] = '_';
                        temp[2][2] = '_';
                        temp[0][0] = ms[i - 1][j - 1];
                        temp[0][1] = ms[i - 1][j];
                        temp[1][1] = ms[i][j];
                        temp[2][1] = ms[i + 1][j];
                        temp[1][0] = ms[i][j - 1];
                        temp[2][0] = ms[i + 1][j - 1];
                        ms[i][j] = fd.proverka(temp);
                    } else {
                        temp[0][2] = ms[i - 1][j + 1];
                        temp[1][2] = ms[i][j + 1];
                        temp[2][2] = ms[i + 1][j + 1];
                        temp[0][0] = ms[i - 1][j - 1];
                        temp[0][1] = ms[i - 1][j];
                        temp[1][1] = ms[i][j];
                        temp[2][1] = ms[i + 1][j];
                        temp[1][0] = ms[i][j - 1];
                        temp[2][0] = ms[i + 1][j - 1];
                        ms[i][j] = fd.proverka(temp);
                    }
                }
            }
            a++;
            c = 0;
            num = 0;
            b = 0;
            System.out.println(a + "-е Поколение");
            for (int i = 0; i < ms.length; i++) {
                for (int j = 0; j < ms[0].length; j++) {
                    System.out.print(ms[i][j] + " ");
                }
                System.out.println();
            }
            if (fd.proverka2(ms,ms1,num,c,b)==1){
                break;
            }
            for (int i = 0; i < ms.length; i++) {
                for (int j = 0; j < ms[0].length; j++) {
                    ms1[i][j] = ms[i][j];
                }
            }
        }
    }
}

